.. CCP4i2 documentation master file, created by
   sphinx-quickstart on Fri Sep  4 11:58:53 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to CCP4i2's documentation!
==================================

.. toctree::
   :maxdepth: 1

   tasks/index
   tutorials/index
   developers/index
   i2run/i2run
   general/tasklist

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
