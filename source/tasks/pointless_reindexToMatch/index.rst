############################
Reindex or change spacegroup
############################

   Sometimes when comparing similar structures, the spacegroup or
   indexing convention may be different even when the crystal packing is
   similar. In this case the symmetry should be expanded or reduced as
   necessary and the indexing convention changed to match the existing
   structure.

   |image1|
   Similarly during the course of a structure solution it may become
   obvious that the spacegroup has been incorrectly determined. For
   example, a symmetry which is present in the data may have been missed
   leading to the selection of a spacegroup with lower symmetry, or a
   higher spacegroup may have been selected due to the presence of
   twinning or pseudo-symmetry. In the latter case, the data processing
   should be repeated to obtain a complete list of reflections. In the
   former case, the reflections can be reduced to the higher symmetry
   asymmetric unit by averaging symmetry equivalent reflections.

   The reindex and change spacegroup task addresses each of theses
   problems. It will reindex a set of observations, and optionally a set
   of Free-R flags, to match a reference structure or set of
   observations.

Input
=====

   The task requires two types of information: the observations (and
   optionally Free-R set) to reindex, and a reference dataset structure
   to which the indexing should conform.

   The reflection data object (and optionally Free-R set) are selected
   **(1)**.

   The spacesgroup and indexing information can come from an existing
   structure, or from a set of observations, or be specified in terms of
   a unit cell and spacegroup. These are selected from the menu **(2)**.
   The corresponding dateset, model or values can then be selected
   **(3)**.

.. |image1| image:: pointless_reindexToMatch_task_1.png
