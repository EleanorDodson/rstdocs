#########################################
Phaser experimental phasing LLG operation
#########################################

   |image1|
   When solving a structure using anomalous data, it can be useful to
   look for (additional) anomalous scatters which can be used to improve
   the phasing calculation, or in order to locate anomalous scattering
   atoms such as sulphurs which may help in building the structure.
   Anomalous scatterers may be located by using the anomalous
   differences in combination which phases from a partial structure, for
   example from molecular replacement.

Input
=====

   In order to generate an optimal anomalous map, the reflection data
   (which must include anomalous pairs of reflections) and a set of
   atomic coordinates (to provide phase information) are required.

   The input data include a set of observations **(1)**, which may be
   anomalous intensities or amplitudes, and a partial model for a
   significant portion of the known scattering density **(2)**. A set of
   resolution limits may optionally be specified to reduce the noise due
   to missing low resolution terms or larger phase errors at high
   resolution.

   The composition of the unit cell must also be specified, to allow for
   the contribution of unaccounted for scattering matter to be
   evaluated. This may be specified in terms of molecular weights or
   sequences. First select the type of information to be provided
   **(3)**, and then provide the required information in the boxes
   below.

Keywords
========

   |image2|
   For documentation of the advanced options, see the `Phaser
   documentation <http://www.phaser.cimr.cam.ac.uk/index.php/Keywords>`__.

.. |image1| image:: phaser_EP_LLG_task_1.png
.. |image2| image:: phaser_EP_LLG_task_2.png
