################################################
Scaling and merging statistics (program AIMLESS)
################################################

Estimation of resolution
^^^^^^^^^^^^^^^^^^^^^^^^

| 

.. figure:: aimless_pipe_report_8.png
   :alt: Figure 8: Resolution statistics

   Figure 8: Resolution statistics

What is the real resolution? The good guides are (a) the correlation
coefficient between random half-datasets CC(ZZZ‚ZZZ), and (b) the
average signal/noise after averaging symmetry-related observations
<<I>/σ(<I>)>, labelled Mn(I)/sd(Mn(I)) in the Aimless table. The default
cut-off on CC( :sub:`1/2`) around 0.3, and on <<I>/σ(<I>)> is ~1.3. Resolution
cut-offs ought to take into account anisotropy of diffraction, which is
not handled gracefully by current software. Aimless analyses anisotropy
in cones around 2 or 3 orthogonal principal directions (depending on the
point group). The final Summary table includes a rough estimate of the
resolution in each direction, as well as overall estimates. It is often
best to integrate the data to a higher resolution and then cut it back
after examining the Report. Note that R\ :sub:`merge` and its relatives
are not good indicators of resolution, as they increase to arbitrary
large values as the intensities get weaker.

.. figure:: aimless_pipe_report_9.png
   :alt: Figure 9: Completeness vs. Resolution

   Figure 9: Completeness vs. Resolution

Figure 8 shows three graphs against resolution which help you to decide
whether the estimated resolution limits are sensible, tabulated in 8(d),
from CC(ZZZ) and Mn(I)/sd(Mn(I)). 8(a) shows CC(ZZZ) (red line) with a
curve fit (pale blue): it also shows CCanom (green line) which shows in
this case that there is a strong anomalous signal (see later). 8(b)
shows the anisotropy, in two directions for this tetragonal example.
8(c) shows I/σI after merging (yellow) and before (blue). Note that by
default XDS integrated into the corners of a square detector, so the
highest resolution data are very incomplete (and the completeness of the
anomalous data is worse) (figure 9)

It is a mistake to cut back the resolution too severely at the data
reduction stage. It makes sense to restrict the resolution in locating
heavy-atom sub-structures, but in refinement and model building even
weak high resolution data can be helpful. Cutting back the resolution
makes your R-factors look better, but is unlikely to improve your model.

| 

Analysis against Batch, radiation damage etc
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. figure:: aimless_pipe_report_10.png
   :alt: Figure 10: Analysis against batch

   Figure 10: Analysis against batch

| 

The Batch Number is the image number for the central part of a spot, if
necessary incremented by a multiple of 1000 if you are merging multiple
sweeps or crystals. The various analyses against Batch allow you to
detect (a) radiation damage, causing a general decay in data quality
with time (radiation dose) (b) individual bad parts of the data, single
bad images or parts where something went wrong: these need further
investigation, examining the images and the integration process (c) if
you are merging data from multiple crystals or sweeps (all called Runs
in AIMLESS), some may be better than others (see below)

Figure 10 shows a straightforward example of radiation damage. With
increasing radiation dose, the relative B-factor becomes more negative
(figure 10b), so that the average scale (red in 10a) increases above the
scale at infinite resolution (blue in 10a). R\ :sub:`merge` tends to
increase (figure 10c), and the estimated maximum resolution gets worse
(figure 10d, roughly estimated as the point at which I/σI falls below
1.0). The cumulative completeness plot (figure 10e) shows that the
second half of the data could be omitted without compomising
completeness, and it would be worth trying structure solution and
refinement with the truncated data as well as the full set.

| 

.. figure:: aimless_pipe_report_badimage.png
   :alt: Figure 11: one bad image

   Figure 11: one bad image

Sometimes there is just a single bad image which should be excluded, as
in figure 11.

| 

.. figure:: aimless_pipe_report_runCC.png
   :alt: Figure 12: run pair correlations

   Figure 12: run pair correlations

With multiple crystals or sweeps, the decision on which part(s) of the
data to include is more complicated. With large numbers of crystals, the
analysis by the program BLEND may be useful. AIMLESS does include  two
other analyses which may be useful:

#. A table of pairwise correlations between runs, presented as a table
   (figure 12) and a graph against resolution
#. If you have a preliminary structure, you can give the calculated
   structure factors as a reference file (using the Input option Use
   reference data in analysis against Batch after scaling). This will
   plot the correlation coefficient and R-factor between the observed
   intensity and F\ :sub:`calc`\ :sup:`2` as a function of Batch, which
   may help to show which parts of the data agree best with the model.
   Of course this has some danger of model bias, so perhaps should be
   used with caution.

Completeness and multiplicity
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Datasets should be complete, as near to 100% as you can manage. Some
loss of completeness can be tolerated in the outermost resolution bins.
Dangers: watch out for bad anomalous completeness (see example left).
Note that in P1 you need a rotation of at least 180°+2θmax (better 360°)
to get complete anomalous data. Note that XDS integrates into the
corners of square detectors, leading to very incomplete data at the
maximum resolution. You should apply a high resolution limit, maybe to
the resolution of the inscribed circle. Detectors with many tiles may
lose significant numbers of spots in the gaps between tiles, leading to
serious incompleteness in low symmetry space groups (P1). You can
complete the data with another dataset collected by rotation about a
different axis (eg with a Kappa offset). You may also be able to recover
some data by accepting "Edge" reflections flagged by MOSFLM (see
Input->Additional options).

Detection of an anomalous signal
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. figure:: aimless_pipe_report_anom.png
   :alt: Figure 13: Anomalous signal detection

   Figure 13: Anomalous signal detection

   Top line: strong anomalous signal. Bottom line: no anomalous (native)

The most useful indicator of the presence of an anomalous signal is the
plot of CC(anom) (green line in the left-hand panels of figure 11).
CC(anom) is the correlation between two random half-dataset values of
(I+ - I-), a similar definition to that for CC(ZZZ). Figure 13 top left
shows typical behaviour of a strong signal, declining with resolution as
the intensities weaken. With multiple datasets (eg MAD), then the
correlation between datasets is also plotted. The correlation
coefficient is essentially the slope of the scatter plot shown in the
central panels of figure 13 (DelAnom scatterplot), showing elongation
along the diagonal at the top, and a random pattern on the bottom. The
right-hand panels are normal probability or Q-Q plots: these have a
slope > 1.0 if the differences are greater than would be expected from
their estimated errors. For the location of the anomalous sub-structure,
you might want to restrict the resolution to the point where CC(anom)
drops below about 0.3, then use the full resolution range for phasing.
Even a weak or undetectable anomalous signal may be useful for locating
anomalous scatterers in difference Fouriers once you have phases (or
better in log-likelihood gradient maps).

| 

Analysis of Standard Deviations
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. figure:: aimless_pipe_report_SDplot.png
   :alt: Figure 14: SD correction plot

   Figure 14: SD correction plot

AIMLESS compares the observed scatter of symmetry-related observations
around the mean with the estimated σ(I), and "corrects" the σ(I) by a
multiplier and a function of <I>. On average, the observed scatter
should equal the estimated standard deviation.

σ'2(I) = SdFac2 {σ2(I)+ SdB <Ih> + (SdAdd <Ih>)2}

where <Ih> is the average intensity of reflection h. By default, the
three parameters SdFac, SdB & SdAdd are determined automatically, with
separate values for fulls & partials, and for each run. The plot in
figure 14 represents the distribution of normalised errors δ =
(I-<Ih>)/σ(I) as a function of <Ih>. This distribution should have a
standard deviation of 1.0, so the plot should be horizontal with a value
= 1.0 everywhere. If it slopes upwards, then σ(I) is too small for large
I, so SdAdd should be increased, or reduced if it slopes downwards. The
example in figure 14 is OK but not particularly good, but if these plots
vary too much from 1.0 then some parameters can be changed in the input,
in Additional Options->override default parameters for each run. Useful
options are (a) making the the parameters the same or similar (ie
restrained equal) for all runs, and (b) fixing the sdB parameter to 0.0.

| 

Statistics
^^^^^^^^^^

*Measures of self-consistency:* these are R-factor or correlation
coefficient statistics which compare symmetry-related or replacate
measurements. They are used in analyses against resolution and against
batch, as described above.

#. Classical R\ :sub:`merge`: ∑\ h\ ∑\ l\ \|I\ hl-<I\ h>|/ ∑∑<I\ h>
#. R\ :sub:`meas`: ∑\ h\ ∑\ l\ (n/n-1)\ 1/2 \|I\ hl-<I\ h>|/∑∑<I\ h>
   Multiplicity-weighted R-factor, a better overall indicator than the
   classic version as it does not increase with multiplicity
#. R\ :sub:`pim`: ∑\ h\ ∑\ l\ (1/n-1)\ 1/2 \|I\ hl-<I\ h>|/∑∑<I\ h>
   Precision-indicating R-factor, more closely related to precision of
   the merged intensity
#. Correlation coefficients, such as CC(ZZZ)
   These have the advantage of well-defined statistical properties (0 no
   correlation, +1 perfect correlation), unlike R-factors

Note that some authorities use a slightly different definition of
R\ :sub:`merge` and its relatives, with ∑∑I\ hl as the denominator
instead of ∑∑<I\ h>. These are likely to have very similar values in
most cases.

References
^^^^^^^^^^

To be added
